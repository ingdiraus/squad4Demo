package com.hackathon.squad4.squad4Demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hackathon.squad4.squad4Demo.model.Stock;

public interface StockRepository extends JpaRepository<Stock, Long>{

}
