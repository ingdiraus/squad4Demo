package com.hackathon.squad4.squad4Demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="stock")
public class Stock {

	@Column(name="stockId")
	@Id
	private Long stockId;
	
	@Column(name="stockName")
	private String stockName;
	
	@Column(name="exchange")
	private String exchange;
	
	@Column(name="stockPrice")
	private double stockPrice;
	
	@Column(name="totalStockPrice")
	private double totalStockPrice;
	
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	public void setExchange(String exchange) {
		this.exchange = exchange;
	}
	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}
	public void setStockPrice(double stockPrice) {
		this.stockPrice = stockPrice;
	}
	public void setTotalStockPrice(double totalStockPrice) {
		this.totalStockPrice = totalStockPrice;
	}
	public String getStockName() {
		return stockName;
	}
	public String getExchange() {
		return exchange;
	}
	public Long getStockId() {
		return stockId;
	}
	public double getStockPrice() {
		return stockPrice;
	}
	public double getTotalStockPrice() {
		return totalStockPrice;
	}
	@Override
	public String toString() {
		return "Stock [stockName=" + stockName + ", exchange=" + exchange + ", stockId=" + stockId + ", stockPrice="
				+ stockPrice + ", totalStockPrice=" + totalStockPrice + "]";
	}
	
	
}
