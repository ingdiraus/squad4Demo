package com.hackathon.squad4.squad4Demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Squad4DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Squad4DemoApplication.class, args);
	}
}
