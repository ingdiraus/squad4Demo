package com.hackathon.squad4.squad4Demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="userstocks")
public class UserStocks {
	@Id
	@Column(name="userId")
	private Long userId;
	
	@Column(name="stockId")
	private Long stockId;
	
	
	@Column(name="stockName")
	private String stockName;
	
	@Column(name="exchange")
	private String exchange;
	
	@Column(name="stockPrice")
	private double stockPrice;
	
	@Column(name="totalStockPrice")
	private double totalStockPrice;	

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getExchange() {
		return exchange;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	public double getStockPrice() {
		return stockPrice;
	}

	public void setStockPrice(double stockPrice) {
		this.stockPrice = stockPrice;
	}

	public double getTotalStockPrice() {
		return totalStockPrice;
	}

	public void setTotalStockPrice(double totalStockPrice) {
		this.totalStockPrice = totalStockPrice;
	}


	@Override
	public String toString() {
		return "UserStocks [userId=" + userId + ", stockName=" + stockName + ", exchange=" + exchange + ", stockPrice="
				+ stockPrice + ", totalStockPrice=" + totalStockPrice + "]";
	}
	
	
}
