package com.hackathon.squad4.squad4Demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.squad4.squad4Demo.dao.StockRepository;
import com.hackathon.squad4.squad4Demo.dao.UserStocksRepository;
import com.hackathon.squad4.squad4Demo.model.Stock;
import com.hackathon.squad4.squad4Demo.model.UserStocks;

@RestController
public class StockService {
	
	@Autowired
	StockRepository stockRepository;
	
	@Autowired
	UserStocksRepository usRepository; 

	@RequestMapping(value="/getAllStocks", method=RequestMethod.GET)
	public List<Stock> getAllStocks() {
		List<Stock> stocks = stockRepository.findAll();
		System.out.println("List of all stocks:::"+stocks.toArray());
		return stocks;
	}
	
	@RequestMapping(value="/getStock/{stockId}", method=RequestMethod.GET)
	public Stock getStock(@PathVariable("stockId") Long stockId) {
		Optional<Stock> stock = stockRepository.findById(stockId);
		System.out.println("stockkkkkkkkkkkkkk:"+stock.get().getStockName());
		return stock.get();
	}
	
	@RequestMapping(value="/saveStocks", method=RequestMethod.POST)
	@ResponseStatus(code = HttpStatus.CREATED)
	public String saveStocks(@RequestBody Stock stock) {
		stockRepository.save(stock);
		System.out.println("stock updated");
		return "success";
	}
	
	@RequestMapping(value="/getQuoteForStock/{stockId}/stocks/{numberOfStocks}", method=RequestMethod.GET)
	public Double getQuoteForStock(@PathVariable("stockId") Long stockId, @PathVariable("numberOfStocks") Long quantity) {
		Optional<Stock> stock = stockRepository.findById(stockId);
		System.out.println("---------"+stock.get().getStockName());
		double brokerage = 0;
		double stockPrice = 0;
		if(stock.isPresent()) {
			stockPrice = stock.get().getStockPrice();
			if(quantity < 500) {
				brokerage = 0.001 * stockPrice * quantity; 
			}else {
				brokerage = (0.001 * stockPrice * 500) + (0.0015 * stockPrice * (quantity-500)) ; 
			}
			System.out.println("getQuoteForStock():"+stock.get().getStockName());
		}
		return brokerage;
	}
	
	
	@RequestMapping(value="/confirmUserStocks/{userId}", method=RequestMethod.POST)
	public String confirmUserStocks(@RequestBody Stock stock, @PathVariable("userId") Long userId) {
		UserStocks us = new UserStocks();
		us.setUserId(userId);
		us.setStockId(stock.getStockId());
		us.setStockName(stock.getStockName());
		us.setExchange(stock.getExchange());
		us.setStockPrice(stock.getStockPrice());
		us.setTotalStockPrice(stock.getTotalStockPrice());
		usRepository.save(us);
		return "SUCCESS";
	}

	
}

