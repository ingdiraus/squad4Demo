package com.hackathon.squad4.squad4Demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Squad4DemoApplicationTests {

	@Test
	public void contextLoads() {
	}

}
